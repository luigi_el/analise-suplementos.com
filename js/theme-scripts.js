/* Main Javascript Object */
var siteApp= {};
(function($) {
	"use strict";
	
	// Set up name space.
	var app= {
		'functions': {},
		'data': {
			'timers': {}
		},
		'models': {}

	};

	app.settings= {
		'cookie_options': {
			'expires': 7,
			'path': '/',
			'domain': 'http://www.analiseemagrecedores.com.br/'
		},
		'sizer_classes': [
			'sizer-xs',
			'sizer-sm',
			'sizer-md',
			'sizer-lg'
		]
	};
	
	if(false) siteApp= app; // Expose entire app for debugging
	siteApp.functions = app.functions; // Expose site-wide functions

	/* Functions */
	
	// jQuery extension for smooth scrolling
	$.fn.scrollView = function() {
		return this.each(function() {
			var time = ($(this).offset().top - $(window).scrollTop())/4;
			$('html, body').animate({
				'scrollTop' : $(this).offset().top
			}, time);
		});
	}
	
	app.functions.wait_for_final_event = function(callback, ms, uniqueId) {
		if(!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if(app.data.timers[uniqueId]) {
			clearTimeout(app.data.timers[uniqueId]);
		}
		app.data.timers[uniqueId] = setTimeout(callback, ms);
	};

	app.functions.setup_screen_width = function() {
		/* Update on a screen resize */
		$(window).on('resize', function() {
			app.functions.wait_for_final_event(function() {
				app.data.screen_size = $('.screen_sizer:visible').attr('data-screen-size');
				$('body').removeClass(app.settings.sizer_classes.join(' '));
				$('body').addClass(app.settings.sizer_classes[app.data.screen_size]);
				/* Functions to fire after a resize event */
				if($('body').hasClass('force_desktop')&&app.data.screen_size>0) {
					$('body').removeClass('force_desktop');
				}
				if($('.eq_height').length) {
					app.functions.equalize_column_height('.eq_height', '.eq_elem');
					app.functions.equalize_column_height('.eq_height', '.eq_elem_2');
				}
				if($('.vert_center').length) {
					app.functions.center_elements_vertically();
				}
				app.functions.image_interchange();
			}, 100, 'setup_screen_width');
		});
		/* Initialize */
		app.data.screen_size = parseInt($('.screen_sizer:visible').attr('data-screen-size'));
		$('body').addClass(app.settings.sizer_classes[app.data.screen_size]);
		app.functions.image_interchange();
	};

	app.functions.image_interchange = function() {
		$('img.interchange').each(function() {
			var interchange_size = 'xs';
			if(app.data.screen_size==1) {
				interchange_size = 'sm';
			} else if(app.data.screen_size==2) {
				interchange_size = 'md';
			} else if(app.data.screen_size==3) {
				interchange_size = 'lg';
			}
			if(interchange_size==='lg') {
				if(!app.functions.has_attribute($(this), 'data-interchange-lg')) {
					interchange_size = 'md';
				}
			}
			if(interchange_size==='md') {
				if(!app.functions.has_attribute($(this), 'data-interchange-md')) {
					interchange_size = 'sm';
				}
			}
			if(interchange_size==='sm') {
				if(!app.functions.has_attribute($(this), 'data-interchange-sm')) {
					interchange_size = 'xs';
				}
			}
			if(interchange_size==='xs') {
				if(!app.functions.has_attribute($(this), 'data-interchange-xs')) {
					interchange_size = '';
				}
			}
			if(interchange_size!=='') {
				$(this).attr('src', $(this).attr('data-interchange-' + interchange_size));
			}
		});
	};

	app.functions.equalize_column_height = function(parent_elem, child_elem) {
		var col_pos;
		var col_pos_old = -1;
		var col_tier = 0;
		/* Mark each column with a tier indicator (signifies same vertical position) */
		$(parent_elem).find(child_elem).each(function() {
			$(this).height('auto'); // Reset height to auto
			col_pos = $(this).offset().top;
			if(col_pos>col_pos_old) {
				col_pos_old = col_pos;
				col_tier += 1;
			}
			$(this).attr('data-col-tier', col_tier);
		});
		/* Loop through each column tier */
		var col_height, col_height_eq;
		for(var i=1;i<=col_tier;i++) {
			col_height_eq = 0;
			/* Identify maximum natural column height */
			$(parent_elem).find(child_elem + '[data-col-tier="' + i + '"]').each(function() {
				col_height = $(this).height();
				col_height_eq = Math.max(col_height_eq, col_height)
			});
			/* Set height of all columns in this tier to the maximum */
			$(parent_elem).find(child_elem + '[data-col-tier="' + i + '"]').each(function() {
				$(this).height(col_height_eq);
			});
		}
	};

	app.functions.center_elements_vertically = function() {
		$('.vert_center').each(function() {
			$(this).css('padding-top', 0).css('padding-bottom', 0);
			var elem_height = $(this).height();
			var parent_height = $(this).parent().height();
			if(parent_height>elem_height) {
				var elem_padding = parseInt((parent_height - elem_height)/2);
				$(this).css('padding-top', elem_padding).css('padding-bottom', elem_padding);
			}
		});
	};

	app.functions.has_attribute = function(elem, attr) {
		var attr = elem.attr(attr);
		return (typeof attr !== typeof undefined)&&(attr!==false)
	};

	app.functions.make_footer_sticky = function() {
		/* Make footer "sticky" */
		var footer= $('footer');
		var pos= footer.position();
		var height= $(window).height();
		height= height - pos.top;
		height= height - footer.height();
		if(height>0) footer.css({'margin-top': height + 'px'});
	};

	/* Run after the document is ready */
	$(document).ready(function() {
		/* Initialization */
		app.functions.setup_screen_width();
		app.functions.make_footer_sticky();
		if(Cookies.get('force_desktop')==1) {
			$('meta[name=viewport]').attr('content', 'width=1200');
			$('body').addClass('force_desktop');
		}

		/** 
		* basic functionality for the off-canvas menu that is shown to 
		* small screen sizes. 
		*/
		$('#mobileNavigation > li > a').on('click', function(e) {
			var that= this;
			var subMenu= $(this).siblings('ul.sub-menu');
			if(subMenu.length>0) {
				if(!($(this).hasClass('chat'))) {
					e.preventDefault();
				}
				if($(subMenu).is(':visible')) {
					// slide up
					$(subMenu).slideUp('fast', function() {
						$(that).removeClass('expanded');
					});
				} else {
					// slide down
					$(subMenu).slideDown('fast', function() {
						$(that).addClass('expanded');
					});
				}
			}
		});

		/* Post-load formatting of all iframes to be responsive */
		if($('iframe.responsive').length>0) {
			$('iframe.responsive').each(function() {
				var thisIframe= $(this).clone();
				var width= thisIframe.attr('width');
				var height= thisIframe.attr('height');
				thisIframe.removeAttr('width').removeAttr('height');
				var newDiv= $('<div></div>').addClass('iframe-sizer').css('padding-bottom', height/width*100 + '%');
				var newIframe= newDiv.append(thisIframe);
				newDiv= $('<div></div>').addClass('iframe-container').css('max-width', width + 'px');
				newIframe= newDiv.append(newIframe);
				$(this).after(newIframe);
				$(this).remove();
			});
		}

		/* Header dropdown functionality */
		$('.nav_horiz').on('click', 'li.menu-item-has-children a', function() {
			var submenu = $(this).next('ul');
			if(submenu.is(':visible')) {
				submenu.slideUp(300);
			} else {
				submenu.slideDown(300);
			}
		});

		/* Prep modals */
		$('a.remote_modal').each(function() {
			var remote_url = $(this).attr('data-href');
			var modal_id = $(this).attr('data-target');
			$(modal_id).attr('data-remote-href', remote_url);
		});

		/* On-demand modal content loader */
		$('div.remote_modal').on('show.bs.modal', function(event) {
			var modal_body = $(event.target).find('.modal-body');
			if(modal_body.html()=='') {
				var remote_url = $(event.target).attr('data-remote-href');
				modal_body.load(remote_url, function(result) {});
			}
		});

		/* Switch to desktop view on mobile devices */
		$('#mobile_switch').on('click', function() {
			if((typeof Cookies.get('force_desktop'))=='undefined') {
				Cookies.set('force_desktop', '1');
			} else {
				Cookies.expire('force_desktop');
			}
			location.reload();
		});

		$('.smooth_scroll').on('click', function(event) {
			event.preventDefault
			var target = $(this).attr('href') || false;
			if(target) {
				$(target).scrollView();
			}
			return false;
		});

		/* Populate shopping cart item count */
		//var cart_id= Cookies.get('UltraCartShoppingCartID');
		//$.ajax({
		//	'url': site_vars.uc_api_url + (cart_id ? '/' + cart_id : ''),
		//	'cache': false,
		//	'headers': {
		//		'X-UC-Merchant-Id': site_vars.mid
		//	},
		//	'dataType': 'json'
		//}).done(function(response, textStatus, jqXHR) {
		//	var intCount = 0;
		//	if(response) {
		//		intCount= response.items.length;
		//		Cookies.set('UltraCartShoppingCartID', response.cartId);
		//	}
		//	$('header span.cart-count').html(intCount);
		//});		
	});

	/* Run after the page is finished loading */
	$(window).load(function() {
		/* Equalize element heights -- This has to happen after all images load */
		if($('.eq_height').length) {
			app.functions.equalize_column_height('.eq_height', '.eq_elem');
			app.functions.equalize_column_height('.eq_height', '.eq_elem_2');
		}
		if($('.vert_center').length) {
			app.functions.center_elements_vertically();
		}
	});
})(jQuery.noConflict());